import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '@utils/ResponsiveScreen';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  topViewStyle: {
    borderTopLeftRadius: wp(5),
    borderTopRightRadius: wp(5),
    height: hp(2),
    width: wp(40),
    backgroundColor: '#b1b6bd',
  },
  parentView: {
    alignItems: 'center',
    flex: 1,
    marginTop: hp(2),
  },
  flaskViewStyle: {
    width: wp(60),
    backgroundColor: '#b1b6bd',
    borderTopLeftRadius: wp(2),
    borderTopRightRadius: wp(2),
  },
  leftRatingView: {
    width: wp(19),
    marginTop: hp(2),
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  rightRatingView: {
    width: wp(19),
    height: hp(60),
    marginTop: hp(2),
  },
  flaskContainer: {
    flexDirection: 'row',
  },
  levelStyle: {
    position: 'absolute',
    width: '100%',
    height: 2,
    backgroundColor: '#05fcdd',
  },
  levelBgColor: {
    backgroundColor: '#f59c1d',
  },
  alignItemsCenter: {
    alignItems: 'center',
  },
  pointerTextStyle: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 14,
    left: '30%',
    position: 'absolute',
  },
  pointerViewStyle: {
    height: 2,
    width: '30%',
    backgroundColor: '#ec1c25',
  },
  readingsLineStyle: {marginTop: 10, height: 2},
  positionAbsolute: {
    position: 'absolute',
  },
});
