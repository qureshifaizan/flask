import {styles} from '@flask/FlaskStyles';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '@utils/ResponsiveScreen';
import PropTypes from 'prop-types';
import React from 'react';

import {Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
const {
  topViewStyle,
  parentView,
  flaskViewStyle,
  flaskContainer,
  leftRatingView,
  rightRatingView,
  levelStyle,
  levelBgColor,
  alignItemsCenter,
  pointerTextStyle,
  pointerViewStyle,
  readingsLineStyle,
  positionAbsolute,
} = styles;
const Flask = props => {
  const {height, level, minimumLevel, maximumLevel, warningLevel} = props;

  const createStripedLines = () => {
    let lines = [];
    for (var i = 0; i < height / 12; i++) {
      const lineBgColor =
        i === minimumLevel || i === maximumLevel || i === warningLevel
          ? '#05fcdd'
          : '#b1b6bd';
      const lineWidth =
        i === minimumLevel || i === maximumLevel || i === warningLevel
          ? wp(6)
          : wp(3);
      lines.push(
        <View
          key={i}
          style={[
            {
              backgroundColor: lineBgColor,
              width: lineWidth,
            },
            readingsLineStyle,
          ]}
        />,
      );
    }
    return lines;
  };
  const filledLevelHeight = level <= height / 12 ? level * 12 : height;
  const filledLevelTop = level <= height / 12 ? height - level * 12 : 0;
  return (
    <SafeAreaView>
      <View style={parentView}>
        <View style={flaskContainer}>
          {/*Left Readings Container*/}
          <View style={[leftRatingView, {height: height}]}>
            <View
              style={{
                height: '100%',
                width: wp(14),
              }}>
              {/*Maximum Level View*/}
              <Text
                style={[
                  {
                    top: height - 15 - maximumLevel * 12,
                  },
                  positionAbsolute,
                  // levelStyle,
                ]}>
                {`Max ${maximumLevel} L`}
              </Text>
              {/*Minimum Level View*/}
              <Text
                style={[
                  {
                    top: height - 15 - minimumLevel * 12,
                  },
                  positionAbsolute,
                ]}>
                {`Min ${minimumLevel} L`}
              </Text>
              {/*Warning Level Text*/}
              <Text
                style={[
                  {
                    top: height - 15 - warningLevel * 12,
                  },
                  positionAbsolute,
                  // levelStyle,
                ]}>
                {`Warning ${warningLevel} L`}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'column-reverse',
                marginTop: hp(2),
                alignItems: 'flex-end',
              }}>
              {createStripedLines()}
            </View>
          </View>

          <View style={alignItemsCenter}>
            {/*Top Notch View*/}
            <View style={topViewStyle} />

            {/*Main Level Container View*/}
            <View
              style={[
                flaskViewStyle,
                {
                  height: height,
                },
              ]}>
              {/*Filled Level View*/}
              <View
                style={[
                  {
                    height: filledLevelHeight,
                    top: filledLevelTop,
                  },
                  levelBgColor,
                ]}
              />
              {/*Maximum Level View*/}
              <View
                style={[
                  {
                    top: height - 2 - maximumLevel * 12,
                  },
                  levelStyle,
                ]}
              />
              {/*Minimum Level View*/}
              <View
                style={[
                  {
                    top: height - 2 - minimumLevel * 12,
                  },
                  levelStyle,
                ]}
              />
              {/*Warning Level View*/}
              <View
                style={[
                  {
                    top: height - 2 - warningLevel * 12,
                  },
                  levelStyle,
                ]}
              />
            </View>
          </View>
          {/*Right pointer View*/}
          <View style={[rightRatingView, {height: height}]}>
            <View
              style={[
                {
                  top: filledLevelTop,
                },
                pointerViewStyle,
              ]}
            />
            <Text
              style={[
                {
                  top: filledLevelTop - 15,
                },
                pointerTextStyle,
              ]}>
              {`Current ${level} L`}
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Flask;

Flask.propTypes = {
  height: PropTypes.number.isRequired,
  minimumLevel: PropTypes.number.isRequired,
  maximumLevel: PropTypes.number.isRequired,
  warningLevel: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
};

Flask.defaultProps = {
  height: hp(60),
  minimumLevel: 5,
  maximumLevel: 40,
  warningLevel: 10,
  level: 5,
};
