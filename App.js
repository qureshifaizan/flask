import React, {Node, useEffect} from 'react';
import Flask from '@flask/Flask';
import {View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from '@utils/ResponsiveScreen';

const App: () => Node = () => {
  return (
    <View style={{flex: 1}}>
      <Flask
        height={hp(80)}
        minimumLevel={10}
        maximumLevel={40}
        warningLevel={17}
        level={30}
      />
    </View>
  );
};

export default App;
